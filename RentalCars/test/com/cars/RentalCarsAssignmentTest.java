package com.cars;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.cars.model.Car;

public class RentalCarsAssignmentTest {
	
	static String response;
	
	@BeforeClass
	public static void init() throws IOException {
		response = Util.readJsonFromFile("test/data/TestResponse.json");
	}
	
	@Test
	public void testBlueTeslas() throws IOException {
		ArrayList<Car> blueTeslas = RentalCarsAssignment.printAllBlueTeslas(response);
		Assert.assertEquals(2, blueTeslas.size());
		Assert.assertEquals("tesla", blueTeslas.get(0).make.toLowerCase());
		Assert.assertEquals("blue", blueTeslas.get(0).metadata.color.toLowerCase());
		Assert.assertEquals("tesla", blueTeslas.get(1).make.toLowerCase());
		Assert.assertEquals("blue", blueTeslas.get(1).metadata.color.toLowerCase());
	}
	
	@Test
	public void testFindLowestRentalCostCars() throws IOException {
		ArrayList<Car> lowestRentalCostCars = RentalCarsAssignment.findLowestRentalCostCars(response);
		Assert.assertEquals(3, lowestRentalCostCars.size());
		Assert.assertEquals("audi", lowestRentalCostCars.get(0).make.toLowerCase());
		Assert.assertEquals("bmw", lowestRentalCostCars.get(1).make.toLowerCase());
		Assert.assertEquals("toyota", lowestRentalCostCars.get(2).make.toLowerCase());
	}
	
	@Test
	public void testFindLowestRentalCostCarsAfterDiscount() throws IOException {
		ArrayList<Car> lowestRentalCostCarsAfterDiscount = RentalCarsAssignment.findLowestRentalCostCarsAfterDiscount(response);
		Assert.assertEquals(1, lowestRentalCostCarsAfterDiscount.size());
		Assert.assertEquals("bmw", lowestRentalCostCarsAfterDiscount.get(0).make.toLowerCase());
		Assert.assertEquals("09AGHY64352J", lowestRentalCostCarsAfterDiscount.get(0).vin);
	}
	
	@Test
	public void testFindHighestGeneratingRevenueCar() throws IOException {
		ArrayList<Car> highestGeneratingRevenueCar = RentalCarsAssignment.findHighestGeneratingRevenueCar(response);
		Assert.assertEquals(1, highestGeneratingRevenueCar.size());
		Assert.assertEquals("09AGHY64352JI", highestGeneratingRevenueCar.get(0).vin);
		Assert.assertEquals("tesla", highestGeneratingRevenueCar.get(0).make.toLowerCase());
	}
}
