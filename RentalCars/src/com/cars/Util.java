package com.cars;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.cars.model.Car;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Util {
	
	public static final ObjectMapper mapper = new ObjectMapper();
	
	public static Car[] getCarsArrayFromJson(String response) throws JsonParseException, JsonMappingException, IOException {
		Car[] cars = mapper.readValue(response, Car[].class);
		return cars;
	}
	
	public static String readJsonFromFile(String filePath) throws IOException {
		return new String(Files.readAllBytes(Paths.get(filePath)));
	}
}
