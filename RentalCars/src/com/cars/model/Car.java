package com.cars.model;

public class Car {
	public String make;
	public String model;
	public String vin;
	public Metadata metadata;
	public PerDayRent perDayRent;
	public Metrics metrics;
}
