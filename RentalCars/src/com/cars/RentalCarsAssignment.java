package com.cars;

import java.io.IOException;
import java.util.ArrayList;

import com.cars.model.Car;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class RentalCarsAssignment {

	public static ArrayList<Car> printAllBlueTeslas(String response)
			throws JsonParseException, JsonMappingException, IOException {
		Car[] cars = Util.getCarsArrayFromJson(response);
		ArrayList<Car> blueTeslas = new ArrayList<>();
		for (int i = 0; i < cars.length; i++) {
			Car car = cars[i];
			if (car.make.equalsIgnoreCase("Tesla")
					&& car.metadata.color.equalsIgnoreCase("Blue")) {
				blueTeslas.add(car);
			}
		}
		return blueTeslas;
	}

	public static ArrayList<Car> findLowestRentalCostCars(String response)
			throws JsonParseException, JsonMappingException, IOException {
		float minRentPrice = Float.MAX_VALUE;
		ArrayList<Car> lowestRentPriceCars = new ArrayList<>();
		Car[] cars = Util.getCarsArrayFromJson(response);
		for (int i = 0; i < cars.length; i++) {
			Car car = cars[i];
			if (car.perDayRent.price < minRentPrice) {
				minRentPrice = car.perDayRent.price;
				lowestRentPriceCars.clear();
				lowestRentPriceCars.add(car);
			} else if (car.perDayRent.price == minRentPrice) {
				lowestRentPriceCars.add(car);
			}
		}
		return lowestRentPriceCars;
	}

	public static ArrayList<Car> findLowestRentalCostCarsAfterDiscount(
			String response) throws JsonParseException, JsonMappingException,
			IOException {
		float minDiscountPrice = Float.MAX_VALUE;
		ArrayList<Car> lowestRentPriceDiscountCars = new ArrayList<>();
		Car[] cars = Util.getCarsArrayFromJson(response);
		for (int i = 0; i < cars.length; i++) {
			Car car = cars[i];
			float price = car.perDayRent.price;
			float discount = car.perDayRent.discount;
			float discountPrice = price - (price * discount / 100);
			if (discountPrice < minDiscountPrice) {
				minDiscountPrice = discountPrice;
				lowestRentPriceDiscountCars.clear();
				lowestRentPriceDiscountCars.add(car);
			} else if (discountPrice == minDiscountPrice) {
				lowestRentPriceDiscountCars.add(car);
			}
		}
		return lowestRentPriceDiscountCars;
	}

	public static ArrayList<Car> findHighestGeneratingRevenueCar(String response)
			throws JsonParseException, JsonMappingException, IOException {
		float higestProfit = Float.MIN_VALUE;
		ArrayList<Car> highestrevenueGenratingCars = new ArrayList<>();
		Car[] cars = Util.getCarsArrayFromJson(response);
		for (int i = 0; i < cars.length; i++) {
			Car car = cars[i];
			float price = car.perDayRent.price;
			float discount = car.perDayRent.discount;
			float discountPrice = price - (price * discount / 100);
			float expense = car.metrics.yoyMaintenanceCost
					+ car.metrics.depreciation;
			float revenue = car.metrics.rentalCount.yearToDate * discountPrice;
			float profit = revenue - expense;

			if (profit > higestProfit) {
				higestProfit = profit;
				highestrevenueGenratingCars.clear();
				highestrevenueGenratingCars.add(car);
			} else if (profit == higestProfit) {
				highestrevenueGenratingCars.add(car);
			}
		}
		return highestrevenueGenratingCars;
	}
}
